\documentclass[11pt]{article}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage[export]{adjustbox}
\usepackage[parfill]{parskip} % Abstand zwischen Absätzen statt Einrückung
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{changepage}
\usepackage[margin=2cm,top=3cm,headheight=15pt]{geometry}
\usepackage[inline]{enumitem}

\pagestyle{fancy}
\lhead{Ausgewählte unentscheidbare Probleme}
\rhead{Seite \thepage\,/\,\pageref{LastPage}}
\cfoot{}

\setcounter{secnumdepth}{1} % no heading numeration below section

\newcommand{\handout}[5]{
  \noindent
  \begin{center}
  \framebox{
    \vbox{
      \hbox to 5.78in { \textbf{#1} \hfill #2 }
      \vspace{4mm}
      \hbox to 5.78in { {\Large \hfill #3  \hfill} }
      \vspace{4mm}
      \hbox to 5.78in { \emph{#4 \hfill #5} }
    }
  }
  \end{center}
  \vspace*{4mm}
}

\newcommand\zb{z.\,B.\ }
\renewcommand\dh{d.\,h.\ }

\newcommand{\lecture}[3]{\handout{Proseminar Theoretische Informatik}{#3}{#1}{#2}{Wolfgang Mulzer}}


\newenvironment{indented}{\begin{adjustwidth}{1cm}{}}{\end{adjustwidth}}

\newtheoremstyle{mitrandundso}% name of the style to be used
  {1.5em}% measure of space to leave above the theorem. E.g.: 3pt
  {1em}% measure of space to leave below the theorem. E.g.: 3pt
  {}% name of font to use in the body of the theorem
  {}% measure of space to indent
  {\bfseries}% name of head font
  {:~}% punctuation between head and body
  { }% space after theorem head; " " = normal interword space
  {}% Manually specify head

% after http://tex.stackexchange.com/a/67308/73880
\newcommand\theoremindent{.75cm}
\makeatletter
\newtheoremstyle{indented}
  {.7em}% space before
  {.7em}% space after
  {\addtolength{\@totalleftmargin}{\theoremindent}
   \addtolength{\linewidth}{-\theoremindent}
   \parshape 1 \theoremindent \linewidth}% body font
  {-\theoremindent}% indent
  {\bfseries}% header font
  {:}% punctuation
  {.5em}% after theorem header
  {}% header specification (empty for default)
\makeatother

\theoremstyle{definition}
\theoremstyle{mitrandundso}
%\theoremstyle{indented}
\newtheorem*{Proof}{Beweis}
\newtheorem{theorem}{Satz}
\newtheorem{corollary}[theorem]{Folgerung}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Beobachtung}
\newtheorem{definition}[theorem]{Definition}

\begin{document}
\thispagestyle{empty}\vspace*{-1.5cm}
\lecture{Ausgewählte unentscheidbare Sprachen}{Marian Sigler, Jakob Köhler}{15.12.15}


\section{Entscheidbarkeit und Semi-Entscheidbarkeit}


\begin{definition}
$L$ ist entscheidbar $\Leftrightarrow$ es gibt eine TM M, so dass für alle $w \in \Sigma$* gilt:
\begin{itemize}
\item []
$w \in L \Rightarrow$ M erreicht q\textsubscript{ja} bei Eingabe $w$\\
$w \notin L \Rightarrow$ M erreicht q\textsubscript{nein} bei Eingabe $w$
\end{itemize}
d.h. M hält bei jeder Eingabe
\end{definition}

\begin{definition} 
L ist semi-entscheidbar $\Leftrightarrow$ es gibt eine TM M, so dass für alle $w \in \Sigma$* gilt:
\begin{itemize}
\item []
$w \in L \Rightarrow$ M erreicht q\textsubscript{ja} bei Eingabe $w$\\
$w \notin L \Rightarrow$ M erreicht q\textsubscript{nein} oder hält nicht bei Eingabe $w$
\end{itemize}
d.h. M hält nur zwangsläufig bei Eingaben $w \in$ L
\end{definition}






\begin{minipage}{0.5\textwidth}
\subsection{Chomsky-Hierarchie}\vspace{.5pt}
\begin{itemize}
\item Die semi-entscheidbaren Sprachen entsprechen in der Chomsky-Hierarchie den Typ-0 Sprachen\\[1pt]
\item Entscheidbare Sprachen sind echte Teilmenge der semi-entscheidbaren Sprachen (H, PCP)\\[1pt]
\item Entscheidbare Sprachen sind echte Obermenge der Typ-1 Sprachen
\end{itemize}\

\end{minipage}
\begin{minipage}{0.5\textwidth}
\hspace{\fill}\includegraphics[width=0.8\textwidth,right]{venn-chomsky}
\end{minipage}



\section{Reduktionen}

Ein Reduktionsbeweis dient allgemein dazu, zu beweisen, dass ein Problem mindestens genauso schwer ist wie ein anderes. Im Folgenden soll mit dieser Beweistechnik die Unentscheidbarkeit verschiedener Probleme gezeigt werden, wir definieren es daher hier nur für diesen Spezialfall.

\begin{definition}
\textbf{Reduktion}

Sei $P$ ein unentscheidbares Problem, $Q$ ein weiteres Problem. Sei $f$ eine (effektiv berechenbare und totale) Funktion, die eine Eingabe $p$ des Problems $P$ in eine Eingabe des Problems $Q$ „umwandelt“: $q := f(p)$.

Wenn nun die Antwort auf eine Eingabe $p$ wahr ist, genau dann wenn die Antwort auf das entsprechende $q$ wahr ist, haben wir eine Möglichkeit gefunden, das Problem $P$ zu lösen, nämlich indem wir seine Eingaben mittels $f$ auf $Q$ abbilden.

Man sagt dann, \emph{$P$ ist reduzierbar auf $Q$}, kurz: $P \le Q$.
\end{definition}

\begin{lemma}\label{lemma:reduktion}
Sei $P$ unentscheidbar, und $P \le Q$. Dann ist auch $Q$ unentscheidbar.
\end{lemma}

\begin{Proof}
Wäre $Q$ entscheidbar, gäbe es eine einfachere Möglichkeit, $P$ zu berechnen, nämlich die obige Abbildung. Damit wäre $P$ entscheidbar, was ein Widerspruch ist.
\end{Proof}






\section{Das Postsche Korrespondenzproblem}

\begin{definition} PCP
\begin{description}
\item[gegeben:] Eine endliche Folge von Wortpaaren $(x_1, y_1), (x_2, y_2), \dots , (x_k, y_k)$ mit $x_i, y_i \in \Sigma^+$
\item[gefragt:] Gibt es eine Folge von Indizes $i_1, i_2, \dots, i_n \in \{1,2,\dots, k\}, n \ge 0$, so dass $x_{i_1} \dots x_{i_n} = y_{i_1} \dots y_{i_n}$?

\end{description}
Dann heißt $i_1, i_2, \dots, i_n$ Lösung des PCP und $ x_{i_1}x_{i_2} \dots x_{i_n} $ Lösungswort.
\end{definition}


\subsection{Semi-Entscheidbarkeit des PCP}
Sei M eine TM, die für immer länger werdende Index-Folgen überprüft, ob sie Lösungen des PCP sind. Dann hält und akzeptiert M nach endlich vielen Schritten, wenn das PCP Lösungen hat. Wenn es keine Lösung gibt, hält M nicht.

\subsection{Skizze für den Beweis der Unentscheidbarkeit des PCP}


\begin{definition}
Halteproblem: H = $\{$(<M>, $w$) | M hält bei Eingabe $w\}$
\end{definition}

\begin{theorem}
Das Halteproblem H ist semi-entscheidbar, aber nicht entscheidbar.
\end{theorem}

\begin{theorem}
Das PCP ist nicht entscheidbar
\end{theorem}

\textbf{zu zeigen:} H $\le$ MPCP $\le$ PCP 


Aus der Unentscheidbarkeit des Halteproblems folgt dann die Unentscheidbarkeit des PCP

\subsection{Das Modifizierte Postsche Korrespondenzproblem}
\begin{samepage}
\begin{definition} MPCP
\begin{description}
\item[gegeben:] wie beim PCP
\item[gefragt:] Gibt es eine Folge von Indizes wie beim PCP, aber mit $i_1 = 1$?

\end{description}
\end{definition}

\end{samepage}

\begin{lemma}
Es gilt: MPCP $\le$ PCP
~\emph{(ohne Beweis)}
\end{lemma}


\subsection*{Reduktion des Halteproblems auf das MPCP}

\begin{lemma}
Es gilt: H $\le$ MPCP
\end{lemma}

\begin{Proof}
Sei M eine Turingmaschine mit M = $\{Z, \Sigma, \Gamma, \delta, z_0, \square, E \}$ und $ w \in \Sigma^* $ ein Eingabewort.

Wir konstruieren nun eine Funktion $f$, die ein solches Paar (M,$w$) in eine Eingabe $(x_1, y_1), \dots,(x_k, y_k)$ für das MPCP überführt. Dabei soll gelten: 
\begin{indented}
M hält auf $w \quad\Leftrightarrow\quad (x_1, y_1), \dots,(x_k, y_k)$ hat eine Lösung mit $i_1 = 1$ (Reduktion)
\end{indented}

Die Funktion $f$ verwendet dabei die folgenden Regeln, um in Abhängigkeit von M und $w$ eine Eingabe für das MPCP zu definieren (Alphabet des MPCP ist $\Gamma \cup Z \cup \{\#\}$):

\begin{itemize}
\item[0.] Das erste Wortpaar ist $(\#,\#z_0 w\#)$
\item[1.] Kopierregeln: $(a,a)$ für alle $a \in \Gamma \cup\{\#\}$
\item[2.] Überführungsregeln:\\
\begin{tabular}{ll}
$(za, z'c)$ & falls $\delta(z,a) = (z',c,N) $ \\
$(za, cz')$ & falls $\delta(z,a) = (z',c,R) $ \\
$(bza, z'bc)$ & falls $\delta(z,a) = (z',c,L)$ für alle $b \in \Gamma$ \\
$(\#za, \#z'\square c)$ & falls $\delta(z,a) = (z',c,L) $ \\
$(z\#, z'c\#)$ & falls $\delta(z,\square) = (z',c,N) $ \\
$(z\#, cz'\#)$ & falls $\delta(z,\square) = (z',c,R) $ \\
$(bz\#, z'bc\#)$ & falls $\delta(z,\square) = (z',c,L)$ für alle $b \in \Gamma$
\end{tabular}
\item[3.] Löschregeln: $(az_e, z_e)$ und $(z_e a, z_e)$ für alle $a \in \Gamma$ und $ z_e \in E$
\item[4.] Abschlussregeln: $(z_e\#\#, \#)$ für alle $z_e \in E $
\end{itemize}

Die Eingabe für das MPCP hat genau dann eine Lösung, wenn M auf $w$ hält. Das dazugehörige Lösungswort hat dann die Form
\begin{indented}
$ \#k_0\#k_1\#\dots \#k_t\#k'_t\#k''_t\#\dots \#z_e\#\# $
\end{indented}
wobei $k_i$ Konfigurationen von M sind, $k_t$ eine Endkonfiguration im Zustand $z_e$ und $k'_t$, $k''_t \dots$ nacheinander aus $k_t$ entstehen, indem die Nachbarsymbole von $z_e$ gelöscht werden.
\hfill \qed
\end{Proof}


\begin{lemma}
Das PCP ist auch mit dem Alphabet $\Sigma = \{0, 1\}$ unentscheidbar.
~\emph{(Ohne Beweis.)}
\end{lemma}



\pagebreak %wir brauchen eh fünf Seiten

\section{Unentscheidbare Grammatik-Probleme}
Ausgehend von der Unentscheidbarkeit des Postschen Korrespondenzproblems können wir beweisen, dass einige Grammatik-Probleme ebenfalls unentscheidbar sind.

\begin{definition}
  Das \textbf{Schnittproblem} zweier Sprachen (definiert durch ihre Grammatiken) ist die Frage, ob es ein Wort gibt, das in beiden Sprachen enthalten ist; anders formuliert, dass der Schnitt dieser Sprachen nicht leer ist.
\end{definition}

\begin{theorem}
  Das Schnittproblem zweier kontextfreier Sprachen ist unentscheidbar.
\end{theorem}

\begin{Proof}
Wir werden im Folgenden das Postsche Korrespondenzproblem (PCP) auf das Schnittproblem reduzieren. Damit ist nach Lemma \ref{lemma:reduktion} die Unentscheidbarkeit gezeigt.

Sei ein PCP gegeben: $\left( (x_1, y_1), \dots, (x_n, y_n) \right)$ über dem Alphabet ${0, 1}$.

Wir konstruieren ein zu diesem PCP passendes Paar von Grammatiken $G_1, G_2$. Ziel ist es, dass nur genau die Worte in den Sprachen beider Grammatiken enthalten sind, die einer Lösung des PCP entsprechen.

Beide Grammatiken haben als Terminalsymbole $\Sigma = {0, 1, \$, a_1, \dots, a_n}$.

$G_1$ hat folgende Regeln:
\begin{align*}
S &~\rightarrow~ A~\$~B \\
A &~\rightarrow~ a_1~A~x_1 ~|~ \dots ~|~ a_n~A~x_n \\
A &~\rightarrow~ a_1~x_1 ~|~ \dots ~|~ a_n~x_n \\
B &~\rightarrow~ \widetilde{y_1}~B~a_1 ~|~ \dots ~|~ \widetilde{y_n}~B~a_n \\
B &~\rightarrow~ \widetilde{y_1}~a_1 ~|~ \dots ~|~ \widetilde{y_n}~a_n
\end{align*}
dabei sind die $a_i$ Terminalsymbole, also unverändert Teil der Grammatik, während die $x_i$ und $y_i$ durch die Worte aus dem gegebenen PCP ersetzt werden. $\widetilde{y_i}$ ist $y_i$ rückwärts gelesen.

$G_2$ hat folgende Regeln:
\begin{align*}
S &~\rightarrow~ a_1~S~a_1~|~\dots~|~a_n~S~a_n~|~T \\
T &~\rightarrow~ 0~T~0~|~1~T~1~|~\$
\end{align*}
alle Wörter in $L(G_2)$ sind also Palindrome (neben weiteren Bedingungen).

Die Wörter aus $L(G_1)$ bestehen vor dem \$ aus $a_i$ und den ihnen entsprechenden $x_i$ aus dem gegebenen PCP; nach dem \$ folgen beliebige $a_i$ und die entsprechenden $y_i$ (jedoch gespiegelt). Der Schnitt mit $L(G_2)$ enthält nur noch solche Wörter, bei denen
\begin{enumerate*}[label=\itshape\alph*\upshape)]
\item in beiden Seiten die selbe Folge von $a_i$ steht und
\item die Konkatenation aller $x_i$ das selbe Wort ist wie die Konkatenation der $y_i$
\end{enumerate*}.

Der Schnitt $L(G_1) \cap L(G_2)$ ist also genau dann nicht leer, wenn es Lösungen für das gegebene PCP gibt. Damit haben wir das PCP auf das Schnittproblem reduziert, das folglich unentscheidbar ist.
\hfill \qed
\end{Proof}



\pagebreak
\begin{theorem}
Die Frage, ob die Schnittmenge der Sprachen zweier kontextfreier Grammatiken unendlich groß ist (also $|L(G_1) \cap L(G_2)| = \infty$), ist unentscheidbar.
\end{theorem}

\begin{Proof}
Wir stellen zunächst fest: Ist ein Wort $w$ eine Lösung eines PCP, so sind es auch beliebige Wiederholungen dieses Wortes. Das heißt: hat ein PCP eine Lösung, so hat es unendlich viele. Die Frage, ob ein PCP unendlich viele Lösungen hat, ist also unentscheidbar.

Wir betrachten wieder die Reduktion aus obigem Beweis. Sie ist ebenfalls eine Reduktion der Frage, ob ein PCP unendlich viele Lösungen hat, auf die Frage, ob die Vereinigung beider Sprachen unendlich viele Wörter enthält. Letztere Frage ist also auch unentscheidbar.
\end{Proof}




\begin{theorem}
Die Frage, ob der Schnitt zweier kontextfreier Sprachen (definiert durch ihre Grammatiken) ebenfalls kontextfrei ist, ist unentscheidbar.
\end{theorem}
\begin{Proof}
Der Schnitt ist, wenn er Worte enthält, nicht kontextfrei (Beweis s.\,u.). Wir haben schon bewiesen, dass es unentscheidbar ist, ob der Schnitt leer ist (dann wäre er kontextfrei). Folglich ist es unentscheidbar, ob er kontextfrei ist.

\textbf{Beweis, dass der Schnitt nicht kontextfrei ist:}
Sei $L_S$ dieser nicht-leere Schnitt. Wir betrachten wieder obige Reduktion, und nehmen an, der Schnitt sei kontextfrei. Dann könnten wir laut dem Pumping-Lemma für kontextfreie Sprachen zwei Teilworte $v, x$ eines Wortes $w$ aus $L_S$ pumpen, und das Ergebnis $w'$ müsste immer noch in $L_S$ sein. Das ist unmöglich, denn:
\begin{itemize}
  \item enthält eines der Teilworte \$, enthielte $w'$ mehrere \$.
  \item enthält eines der Teilworte sowohl Zeichen aus dem $a_i$-Block des Wortes als auch aus dem durch die $x_i$ bzw. $y_i$ erzeugten, nur 0 und 1 enthaltenden Block, hat das gepumpte Wort $w'$ nicht mehr die von $G_2$ verlangte Form ($\{a_i\}^*\{0,1\}^*\$\{0,1\}^*\{a_i\}^*$) und ist damit auch nicht in $L_S$.
  \item ansonsten besteht eines der Teilworte nur aus dem $a_i$-Block oder nur aus dem ${0,1}$-Block. Pumpt man es, passen diese Blöcke nicht mehr zueinander, $w'$ hat also nicht mehr die vom $G_1$ geforderte Form und ist damit auch nicht in $L_S$.
\end{itemize}

(Da wir durch Wiederholung einer Lösung beliebig lange Wörter erzeugen können, lässt sich dieser Widerspruch für jede gegebene Pumping-Zahl zeigen.)
\hfill \qed
\end{Proof}








\section*{Literatur}
\begin{itemize}
  \item  Uwe Schöning. \emph{Theoretische Informatik – kurz gefasst}.\\
         Spektrum Akademischer Verlag, 5. Auflage, 2008.
  \item  Katrin Erk, Lutz Priese. \emph{Theoretische Informatik. Eine umfassende Einführung}.\\
         Springer-Verlag, 3. Auflage, 2008.
\end{itemize}





\end{document}
